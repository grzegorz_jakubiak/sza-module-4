package com.dbauth.token;

import com.dbauth.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class VerificationToken {

    @Id
    private UUID tokenValue;

//    private UUID userId;

    @OneToOne
    @JoinColumn(name = "userId")
    private User user;
}
