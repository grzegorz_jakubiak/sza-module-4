package com.dbauth.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class CustomPasswordEncoder implements PasswordEncoder {

    private final int shift;

    public CustomPasswordEncoder(@Value("${shift:8}") String shift) {
        this.shift = Integer.valueOf(shift).intValue();
    }

    @Override
    public String encode(CharSequence rawPassword) {
        StringBuilder encodedPassword = new StringBuilder();
        for (int i = 0; i < rawPassword.length(); i++) {
            if (Character.isUpperCase(rawPassword.charAt(i))) {
                char ch = (char) (((int) rawPassword.charAt(i) +
                         - 65) % 26 + 65);
                encodedPassword.append(ch);
            } else {
                char ch = (char) (((int) rawPassword.charAt(i) +
                        shift - 97) % 26 + 97);
                encodedPassword.append(ch);
            }
        }
        return encodedPassword.toString();
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        boolean result = false;
        if (encode(rawPassword).equals(encodedPassword)) {
            result = true;
        }
        return result;
    }
}
