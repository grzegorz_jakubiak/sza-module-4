package com.dbauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbAuthApplication.class, args);
    }

}
